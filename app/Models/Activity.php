<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];

    protected $casts = [
        'changes' => 'array',
    ];

    /**
     * The subject of an activity.
     */
    public function subject()
    {
        return $this->morphTo();
    }

    /**
     * User the activity belongs to.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
