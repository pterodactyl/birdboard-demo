<?php

namespace App\Models;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory, RecordsActivity;

    protected $guarded = [];

    protected static $recordableEvents = ['created', 'updated'];

    protected $casts = [
        'owner_id' => 'integer',
    ];

    /**
     * URI path the project accessed by.
     *
     * @return string
     */
    public function path(): string
    {
        return '/projects/' . $this->id;
    }

    /**
     * User the project belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Collection of the tasks belongs to the project.
     *
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }

    /**
     * Add a task to the project.
     *
     * @param string $body
     * @return \Illuminate\Database\Eloquent\Model|\App\Models\Task
     */
    public function addTask(string $body)
    {
        return $this->tasks()->create(['body' => $body]);
    }

    /**
     * Get all the activity for the project.
     *
     * @return HasMany
     */
    public function activity()
    {
        return $this->hasMany(Activity::class)->latest();
    }

    /**
     * Add a user to the project members list.
     *
     * @param User $user
     */
    public function invite(User $user)
    {
        if ($this->owner_id !== $user->id) {
            $this->members()->attach($user);
        }
    }

    /**
     * Users that belongs to the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'project_members');
    }
}
