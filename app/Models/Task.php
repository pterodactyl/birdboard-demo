<?php

namespace App\Models;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Project $project
 * @property bool $is_completed
 */
class Task extends Model
{
    use HasFactory, RecordsActivity;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];

    /**
     * The relationships that should be touched on save.
     *
     * @var string[]
     */
    protected $touches = ['project'];

    protected static $recordableEvents = ['created', 'deleted'];

    /**
     * The attributes that should be cast.
     *
     * @var string[]
     */
    protected $casts = [
        'is_completed' => 'boolean',
    ];

    /**
     * Project the task belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Mark the task as completed.
     */
    public function complete()
    {
        if ($this->is_completed) {
            return;
        }

        $this->is_completed = true;
        $this->saveQuietly();

        $this->recordActivity('completed_task');
    }

    /**
     * Mark the task as incompleted.
     */
    public function incomplete()
    {
        if (!$this->is_completed) {
            return;
        }

        $this->is_completed = false;
        $this->saveQuietly();

        $this->recordActivity('incompleted_task');
    }

    /**
     * Check the task is completed.
     */
    public function completed()
    {
        return $this->is_completed;
    }
}
