<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can edit the project.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function edit(User $user, Project $project)
    {
        return $user->id === $project->owner_id
            || $project->members()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function update(User $user, Project $project)
    {
        return $user->id === $project->owner_id
            || $project->members()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Determine whether the user can view the project.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function view(User $user, Project $project)
    {
        return $user->id === $project->owner_id
            || $project->members()->where('user_id', $user->id)->count() > 0;
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function destroy(User $user, Project $project)
    {
        return $user->id === $project->owner_id;
    }

    /**
     * Determine whether the user can invite another user to the project team.
     *
     * @param User $user
     * @param Project $project
     */
    public function invite(User $user, Project $project)
    {
        return $user->id === $project->owner_id;
    }
}
