<?php

namespace App\Traits;

use App\Models\Activity;
use Illuminate\Support\Arr;

trait RecordsActivity
{
    /**
     * The model's old attributes.
     *
     * @var array
     */
    public $oldAttributes = [];

    /**
     * Boot the trait.
     */
    public static function bootRecordsActivity()
    {
        static::updating(function (self $model) {
            $model->oldAttributes = $model->getOriginal();
        });

        foreach (self::recordableEvents() as $event) {
            static::$event(function (self $model) use ($event) {
                $model->recordActivity(
                    $model->activityDescription($event)
                );
            });
        }
    }

    protected function activityDescription($description)
    {
        return $description . '_' . strtolower(class_basename($this));
    }

    public function recordActivity($description)
    {
        return $this->activity()->create([
            'description' => $description,
            'changes'     => $this->activityChanges(),
            'project_id'  => class_basename($this) === 'Project' ? $this->id : $this->project_id,
            'user_id' => $this->activityOwner()->id,
        ]);
    }

    protected function activityOwner()
    {
        if (auth()->check()) {
            return auth()->user();
        }

        $project = $this->project ?? $this;

        return $project->owner;
    }

    protected function activityChanges(): ?array
    {
        if ($this->wasChanged()) {
            return [
                'before' => Arr::except(array_diff($this->oldAttributes, $this->getAttributes()), 'updated_at'),
                'after'  => Arr::except($this->getChanges(), 'updated_at'),
            ];
        }

        return null;
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    /**
     * @return string[]
     */
    protected static function recordableEvents(): array
    {
        if (isset(static::$recordableEvents)) {
            return static::$recordableEvents;
        }

        return ['created', 'updated', 'deleted'];
    }
}
