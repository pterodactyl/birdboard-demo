<?php

namespace App\Http\Controllers;

use App\Http\Requests\InviteUserRequest;
use App\Models\Project;

class ProjectInvitationsController extends Controller
{
    public function store(Project $project, InviteUserRequest $request)
    {
        $project->invite($request->invitee());

        return redirect($project->path());
    }
}
