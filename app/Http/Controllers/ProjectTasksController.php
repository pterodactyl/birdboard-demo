<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectTask;
use App\Models\Project;
use App\Models\Task;

class ProjectTasksController extends Controller
{
    public function store(Project $project, StoreProjectTask $request)
    {
        $project->addTask($request->validated()['body']);

        return redirect($project->path());
    }

    public function destroy(Task $task)
    {
        $this->authorize('destroy', $task);

        $task->delete();

        return redirect($task->project->path());
    }
}
