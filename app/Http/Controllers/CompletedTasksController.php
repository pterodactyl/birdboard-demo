<?php

namespace App\Http\Controllers;

use App\Models\Task;

class CompletedTasksController extends Controller
{
    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Task $task)
    {
        $this->authorize('update', $task);

        $task->complete();

        return redirect($task->project->path());
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Task $task)
    {
        $this->authorize('update', $task);

        $task->incomplete();

        return redirect($task->project->path());
    }
}
