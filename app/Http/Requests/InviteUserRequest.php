<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class InviteUserRequest extends FormRequest
{
    protected $errorBag = 'invitations';

    public function rules()
    {
        return [
            'email' => 'required|exists:users,email',
        ];
    }

    public function messages() {
        return [
            'email.*' => 'The email address should be a valid birdboard account',
        ];
    }

    public function authorize()
    {
        return $this->user()->can('invite', $this->route('project'));
    }

    public function invitee()
    {
        $email = $this->validated()['email'];

        return User::where('email', $email)->firstOrFail();
    }
}
