<?php

namespace App\Http\Requests;

use App\Models\Project;
use Illuminate\Foundation\Http\FormRequest;

class StoreProjectTask extends FormRequest
{
    public function rules()
    {
        return [
            'body' => 'required',
        ];
    }

    public function authorize()
    {
        return $this->user()->id === $this->project()->owner_id
            || $this->project()->members()->where('user_id', $this->user()->id)->count() > 0;
    }

    /**
     * @return \Illuminate\Routing\Route|object|string|null|Project
     */
    private function project()
    {
        return $this->route('project');
    }
}
