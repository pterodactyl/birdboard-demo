<?php

namespace Tests\Feature;

use App\Models\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageProjectsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function a_user_can_create_a_project()
    {
        $this->signIn();

        $attributes = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'notes' => 'General notes here',
        ];

        $this->get('/create')->assertStatus(200);
        $this->post('/projects', $attributes)->assertRedirect('/');
        $this->assertDatabaseHas('projects', $attributes);
        $this->get('/')->assertSee($attributes['title']);
        $this->get('/projects/1')
            ->assertSee($attributes['title'])
            ->assertSee($attributes['description'])
            ->assertSee($attributes['notes']);
    }

    /** @test */
    public function a_user_can_update_a_project()
    {
        $project = Project::factory()->create();

        $newAttributes = [
            'title' => 'Updated title',
            'description' => 'Updated description',
            'notes' => 'General notes here',
        ];

        $this->actingAs($project->owner)
            ->patch($project->path(), $newAttributes)
            ->assertRedirect($project->path());

        $this->assertDatabaseHas('projects', $newAttributes);
        $this->get($project->path())
            ->assertSee($newAttributes['title'])
            ->assertSee($newAttributes['description'])
            ->assertSee($newAttributes['notes']);
    }

    /** @test */
    public function a_user_cannot_update_projects_of_others()
    {
        $this->signIn();

        $project = Project::factory()->create();

        $this->patch($project->path(), ['notes' => 'General notes here'])->assertStatus(403);
        $this->assertDatabaseMissing('projects', ['notes' => 'General notes here']);
    }

    /** @test */
    public function a_projects_requires_a_title()
    {
        $this->signIn();
        $attributes = Project::factory()->raw(['title' => '']);

        $this->post('/projects', $attributes)->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_project_requires_a_description()
    {
        $this->signIn();
        $attributes = Project::factory()->raw(['description' => '']);

        $this->post('/projects', $attributes)->assertSessionHasErrors('description');
    }

    /** @test */
    public function a_user_can_view_own_projects()
    {
        $project = Project::factory()->create();

        $this->actingAs($project->owner)->get($project->path())
            ->assertSee($project->title)
            ->assertSee($project->description);
    }

    /** @test */
    public function an_authenticated_user_cannot_view_projects_of_others()
    {
        $this->signIn();

        $project = Project::factory()->create();

        $this->get($project->path())->assertStatus(403);
    }

    /** @test */
    public function guest_cannot_manage_projects()
    {
        $project = Project::factory()->create();

        $this->get('/create')->assertRedirect('login');
        $this->post('/projects', $project->toArray())->assertRedirect('login');
        $this->get('/')->assertRedirect('login');
        $this->get($project->path())->assertRedirect('login');
        $this->get($project->path() . '/edit')->assertRedirect('login');
    }

    /** @test */
    public function a_user_can_update_a_project_notes()
    {
        $project = Project::factory()->create();

        $this->actingAs($project->owner)
            ->patch($project->path(), ['notes' => 'General notes here'])
            ->assertRedirect($project->path());
        $this->assertDatabaseHas('projects', ['notes' => 'General notes here']);
    }

    /** @test */
    public function a_user_can_delete_a_project()
    {
        $project = Project::factory()->create();
        $this->actingAs($project->owner);

        $this->delete($project->path())->assertRedirect('/');
        $this->assertDatabaseMissing('projects', ['title' => $project->title]);
    }

    /** @test */
    public function only_project_owner_can_delete_a_project()
    {
        $this->signIn();
        $project = Project::factory()->create();

        $this->delete($project->path())->assertForbidden();
        $this->assertDatabaseHas('projects', ['title' => $project->title]);
    }

    /** @test */
    public function guests_cannot_delete_a_project()
    {
        $project = Project::factory()->create();

        $this->delete($project->path())->assertRedirect('login');
        $this->assertDatabaseHas('projects', ['title' => $project->title]);
    }
}
