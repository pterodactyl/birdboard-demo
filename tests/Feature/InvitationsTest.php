<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvitationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_project_owner_can_invite_users()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $randomUser = User::factory()->create();

        $this->post($project->path() . '/invitations', ['email' => $randomUser->email])
            ->assertForbidden();
    }

    /** @test */
    public function the_invited_email_address_must_be_associated_with_a_valid_birdboard_account()
    {
        $project = Project::factory()->create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/invitations', ['email' => 'notexisting@email.com'])
            ->assertSessionHasErrors('email', null, 'invitations');
    }

    /** @test */
    public function a_project_can_invite_a_user()
    {
        $this->withoutExceptionHandling();

        $project = Project::factory()->create();
        $invitee = User::factory()->create();

        $this
            ->actingAs($project->owner)
            ->post($project->path() . '/invitations', ['email' => $invitee->email])
            ->assertRedirect($project->path());
        $this->assertCount(1, $project->members);
    }

    /** @test */
    public function a_users_can_see_projects_they_have_been_invited_to()
    {
        $this->withoutExceptionHandling();

        $project = tap(Project::factory()->create())
            ->invite($invitee = User::factory()->create());

        $this->actingAs($invitee)->get('/')->assertSee($project->title);
        $this->get($project->path())->assertSee($project->title);
    }

    /** @test */
    public function an_invited_users_can_update_the_project()
    {
        $this->withoutExceptionHandling();
        $project = tap(Project::factory()->create())->invite($this->signIn());

        $this->get($project->path() . '/edit')->assertOk();
        $this->patch($project->path(), ['title' => 'Changed'])->assertRedirect($project->path());
        $this->assertDatabaseHas('projects', ['title' => 'Changed']);
    }
}
