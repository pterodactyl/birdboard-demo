<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_project_can_have_tasks()
    {
        $project = Project::factory()->create();

        $this->actingAs($project->owner);

        $this->post($project->path() . '/tasks', ['body' => 'Lorem ipsum']);
        $this->get($project->path())->assertSee('Lorem ipsum');
    }

    /** @test */
    public function a_task_requires_a_body()
    {
        $project = Project::factory()->create();
        $attributes = Task::factory()->raw(['body' => '']);

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', $attributes)
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function only_the_owner_of_a_project_may_add_tasks()
    {
        $this->signIn();

        $project = Project::factory()->create();
        $attributes = Task::factory()->raw(['body' => 'Test task']);

        $this->post($project->path() . '/tasks', $attributes)->assertStatus(403);
        $this->assertDatabaseMissing('tasks', ['body' => 'Test task']);
    }

    /** @test */
    public function guest_cannot_manage_tasks()
    {
        $task = Task::factory()->create(['body' => 'New task']);

        $this->patch('/tasks/completed/' . $task->id)->assertRedirect('login');
        $this->assertDatabaseMissing('tasks', ['is_completed' => true]);

        $task->complete();

        $this->delete('/tasks/completed/' . $task->id)->assertRedirect('login');
        $this->assertDatabaseMissing('tasks', ['is_completed' => false]);

        $this->delete('/tasks/' . $task->id)->assertRedirect('login');
        $this->assertDatabaseHas('tasks', ['body' => 'New task']);
    }

    /** @test */
    public function an_authenticated_user_can_update_a_task()
    {
        $task = Task::factory()->create();

        $this->actingAs($task->project->owner);

        $this->patch('/tasks/completed/' . $task->id)->assertRedirect($task->project->path());
        $this->assertDatabaseHas('tasks', ['is_completed' => true]);

        $this->delete('/tasks/completed/' . $task->id)->assertRedirect($task->project->path());
        $this->assertDatabaseHas('tasks', ['is_completed' => false]);
    }

    /** @test */
    public function an_authenticated_user_cannot_update_tasks_of_others()
    {
        $this->signIn();

        $task = Task::factory()->create();

        $this->patch('/tasks/completed/' . $task->id)->assertStatus(403);
        $this->assertDatabaseMissing('tasks', ['is_completed' => true]);

        $task->complete();

        $this->delete('/tasks/completed/' . $task->id)->assertStatus(403);
        $this->assertDatabaseMissing('tasks', ['is_completed' => false]);

        $this->delete('/tasks/' . $task->id)->assertStatus(403);
        $this->assertDatabaseHas('tasks', ['body' => $task->body]);
    }

    /** @test */
    public function an_authenticated_user_can_delete_task()
    {
        $task = Task::factory()->create();

        $this->actingAs($task->project->owner)
            ->delete('/tasks/' . $task->id)
            ->assertRedirect($task->project->path());
        $this->assertDatabaseMissing('tasks', ['body' => $task->body]);
    }

    /** @test */
    public function a_project_member_can_manage_tasks()
    {
        $project = tap(Project::factory()->create())->invite($this->signIn());

        $this->post($project->path() . '/tasks', ['body' => 'New task'])->assertRedirect($project->path());
        $this->assertDatabaseHas('tasks', ['body' => 'New task']);

        $task = $project->tasks[0];

        $this->patch('/tasks/completed/' . $task->id)->assertRedirect($project->path());
        $this->assertDatabaseHas('tasks', ['is_completed' => true]);

        $this->delete('/tasks/completed/' . $task->id)->assertRedirect($project->path());
        $this->assertDatabaseHas('tasks', ['is_completed' => false]);

        $this->delete('/tasks/' . $task->id)->assertRedirect($task->project->path());
        $this->assertDatabaseMissing('tasks', ['body' => $task->body]);
    }
}
