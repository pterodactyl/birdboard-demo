<?php

namespace Tests\Feature;

use App\Models\Activity;
use App\Models\Project;
use App\Models\Task;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TriggerActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function creating_a_project_records_activity()
    {
        $project = Project::factory()->create();

        $this->assertCount(1, $project->activity);

        tap($project->activity->last(), function (Activity $activity) {
            $this->assertEquals('created_project', $activity->description);
            $this->assertNull($activity->changes);
        });
    }

    /** @test */
    public function updating_a_project_records_activity()
    {
        $project = Project::factory()->create();
        $originalTitle = $project->title;

        $project->update(['title' => 'Changed']);

        $this->assertCount(2, $project->activity);
        tap($project->activity->last(), function (Activity $activity) use ($originalTitle) {
            $this->assertEquals('updated_project', $activity->description);

            $expected = [
                'before' => ['title' => $originalTitle],
                'after' => ['title' => 'Changed'],
            ];

            $this->assertEquals($expected, $activity->changes);
        });
    }

    /** @test */
    public function creating_a_new_task_records_project_activity()
    {
        $project = Project::factory()->create();
        $project->addTask('New task');

        $this->assertCount(2, $project->activity);

        tap($project->activity->last(), function (Activity $activity) {
            $this->assertEquals('created_task', $activity->description);
            $this->assertInstanceOf(Task::class, $activity->subject);
            $this->assertEquals('New task', $activity->subject->body);
        });
    }

    /** @test */
    public function completing_a_task_records_project_activity()
    {
        $task = Task::factory()->create();

        $this->actingAs($task->project->owner)
            ->patch('/tasks/completed/' . $task->id);

        $this->assertCount(3, $task->project->activity);
        tap($task->project->activity->last(), function (Activity $activity) use ($task) {
            $this->assertEquals('completed_task', $activity->description);
            $this->assertInstanceOf(Task::class, $activity->subject);
            $this->assertEquals($task->body, $activity->subject->body);
        });
    }

    /** @test */
    public function incompleting_a_task_records_project_activity()
    {
        $task = Task::factory()->create();

        $this->actingAs($task->project->owner)
            ->patch('/tasks/completed/' . $task->id);

        $this->assertCount(3, $task->project->activity);
        $this->assertEquals('completed_task', $task->project->activity->last()->description);

        $this->delete('/tasks/completed/' . $task->id);
        $task->project->refresh();

        $this->assertCount(4, $task->project->activity);
        $this->assertEquals('incompleted_task', $task->project->activity->last()->description);
    }

    /** @test */
    public function deleting_a_task_records_project_activity()
    {
        $task = Task::factory()->create();

        $task->delete();

        $this->assertCount(3, $task->project->activity);
        $this->assertEquals('deleted_task', $task->project->activity->last()->description);
    }

    /** @test */
    public function completing_a_task_twice_records_only_one_activity()
    {
        $task = Task::factory()->create();

        $task->complete();
        $task->complete();

        $this->assertCount(3, $task->project->activity);
    }

    /** @test */
    public function incompleting_a_task_twice_records_only_one_activity()
    {
        $task = Task::factory()->create();

        $task->complete();
        $task->incomplete();
        $task->incomplete();

        $this->assertCount(4, $task->project->activity);
    }
}
