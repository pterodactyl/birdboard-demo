<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        $project = Project::factory()->create();

        $path = $project->path();

        $this->assertEquals('/projects/' . $project->id, $path);
    }

    /** @test */
    public function a_project_belongs_to_an_owner()
    {
        $project = Project::factory()->create();

        $this->assertInstanceOf(User::class, $project->owner);
    }

    /** @test */
    public function it_can_add_a_task()
    {
        $project = Project::factory()->create();

        $task = $project->addTask('Test task');

        $this->assertCount(1, $project->tasks);
        $this->assertTrue($project->tasks->contains($task));
    }

    /** @test */
    public function it_touches_updated_at_field_when_the_new_task_is_added()
    {
        $dateTime = now()->subDay();
        $project = Project::factory()->create([
            'created_at' => $dateTime,
            'updated_at' => $dateTime,
        ]);

        $project->addTask('New task');
        $project->refresh();

        $this->assertTrue($dateTime->lessThan($project->updated_at));
    }

    /** @test */
    public function it_can_invite_a_user()
    {
        $project = Project::factory()->create();

        $project->invite($invitee = User::factory()->create());

        $this->assertTrue($project->members->contains($invitee));
    }

    /** @test */
    public function it_cannot_invite_project_owner()
    {
        $project = Project::factory()->create();
        $project->invite($project->owner);

        $this->assertCount(0, $project->members);
    }
}
