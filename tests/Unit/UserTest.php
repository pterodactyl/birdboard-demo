<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_projects()
    {
        $user = User::factory()->create();

        $this->assertInstanceOf(Collection::class, $user->projects);
    }

    public function it_has_accessible_projects()
    {
        [$alice, $bob, $eva] = User::factory(3)->create();

        $project = Project::factory()->for($alice)->create();
        $project->invite($bob);

        $this->assertCount(1, $alice->accessibleProjects);
        $this->assertCount(1, $bob->accessibleProjects);
        $this->assertCount(0, $eva->accessibleProjects);
    }
}
