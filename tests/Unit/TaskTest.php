<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_project()
    {
        $task = Task::factory()->create();

        $this->assertInstanceOf(Project::class, $task->project);
    }

    /** @test */
    public function it_can_be_completed()
    {
        $task = Task::factory()->for(Project::factory())->create();

        $task->complete();
        $task->refresh();

        $this->assertTrue($task->completed());
    }

    /** @test */
    public function it_can_be_incompleted_back()
    {
        $task = Task::factory()->create();

        $task->complete();
        $task->refresh();
        $task->incomplete();
        $task->refresh();

        $this->assertFalse($task->completed());
    }
}
