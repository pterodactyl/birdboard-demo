<?php

use App\Http\Controllers\CompletedTasksController;
use App\Http\Controllers\ProjectInvitationsController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\ProjectTasksController;
use App\Models\Activity;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {
    // Project routes
    Route::get('/', [ProjectsController::class, 'index']);
    Route::get('/create', [ProjectsController::class, 'create']);
    Route::get('/projects/{project}', [ProjectsController::class, 'show']);
    Route::post('/projects', [ProjectsController::class, 'store']);
    Route::patch('/projects/{project}', [ProjectsController::class, 'update']);
    Route::get('/projects/{project}/edit', [ProjectsController::class, 'edit']);
    Route::delete('/projects/{project}', [ProjectsController::class, 'destroy']);

    // Project tasks routes
    Route::post('/projects/{project}/tasks', [ProjectTasksController::class, 'store']);
    Route::patch('/tasks/completed/{task}', [CompletedTasksController::class, 'store']);
    Route::delete('/tasks/completed/{task}', [CompletedTasksController::class, 'destroy']);
    Route::delete('/tasks/{task}', [ProjectTasksController::class, 'destroy']);

    // Invitations
    Route::post('/projects/{project}/invitations', [ProjectInvitationsController::class, 'store']);
});
