<?php

use App\Models\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->boolean('is_completed')->default(false);
            $table->text('body');
            $table->foreignIdFor(Project::class);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
