@csrf

<div class="row mb-4">
    <div class="col">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" placeholder="Title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title', $project->title) }}">

        @error('title')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="row">
    <div class="col">
        <label for="description">Description</label>
        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" placeholder="Description">{{ old('description', $project->description) }}</textarea>

        @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="row mt-4">
    <div class="col">
        <button type="submit" class="btn btn-primary">{{ $submitText }}</button>
    </div>
</div>
