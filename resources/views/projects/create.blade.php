@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/projects" method="post">
            @include('projects._form', [
                'submitText' => 'Create project',
                'project' => new App\Models\Project(),
            ])
        </form>
    </div>
@endsection
