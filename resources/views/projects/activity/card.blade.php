<div class="activity-feed">
    @foreach ($project->activity as $activity)
        <div class="feed-item">
            <div class="date">{{ $activity->created_at->diffForHumans() }}</div>
            <div class="text">
                @includeIf('projects.activity.' . $activity->description)
            </div>
        </div>
    @endforeach
</div>
