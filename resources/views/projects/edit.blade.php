@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ $project->path() }}" method="post">
            @method('patch')

            @include('projects._form', [
                'submitText' => 'Update project',
            ])
        </form>

    </div>
@endsection
