@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col">
                <div class="d-flex">
                    <nav aria-label="breadcrumb" class="me-4">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Projects</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $project->title }}</li>
                        </ol>
                    </nav>

                    <img width="28"
                         height="28"
                         class="avatar"
                         src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}?s=24"
                         alt="{{ Auth::user()->name }}"
                         data-bs-toggle="tooltip"
                         title="{{ Auth::user()->name }} (You)"
                    >

                    @foreach ($project->members as $member)
                        <img width="28"
                             height="28"
                             class="avatar"
                             src="https://gravatar.com/avatar/{{ md5($member->email) }}?s=24"
                             alt="{{ $member->name }}"
                             data-bs-toggle="tooltip"
                             title="{{ $member->name }}"
                        >
                    @endforeach
                </div>
            </div>
            <div class="col text-end">
                <form action="{{ $project->path() }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete the project?');">
                    @csrf
                    @method('delete')

                    <input type="submit" class="btn btn-danger" value="Delete project">
                </form>

                <a href="{{ $project->path() }}/edit" class="btn btn-primary">Edit project</a>
            </div>
        </div>


        <div class="row">
            <div class="col-8">

                <h6>Tasks</h6>

                <div class="list-group mb-3">
                    @foreach ($project->tasks as $task)
                        <div class="list-group-item">
                            <form action="/tasks/completed/{{ $task->id }}" method="post" class="project-task d-flex justify-content-between">
                                @csrf
                                @method($task->completed() ? 'delete' : 'patch')

                                <div>
                                    <input class="form-check-input me-1" type="checkbox" onchange="this.form.submit();"{{ $task->completed() ? ' checked' : '' }}>
                                    <span{!! $task->completed() ? ' class="text-decoration-line-through"' : '' !!}>{{ $task->body }}</span>
                                </div>

                                <a href="#" onclick="event.preventDefault(); if (confirm('Do you really want to delete the task?')) document.forms['delete-task-{{ $task->id }}'].submit();" class="text-danger text-uppercase fw-bold text-decoration-none project-task-delete-button"><small>delete</small></a>
                            </form>

                            <form class="d-none" action="/tasks/{{ $task->id }}" method="post" name="delete-task-{{ $task->id }}">
                                @csrf
                                @method('delete')
                            </form>
                        </div>
                    @endforeach

                    <form action="{{ $project->path() }}/tasks" method="post" class="list-group-item d-flex">
                        @csrf

                        <div class="input-group input-group-sm">
                            <input class="form-control" type="text" placeholder="Enter new task here" name="body" required>
                            <button type="submit" class="btn btn-sm btn-primary">GO!</button>
                        </div>
                    </form>
                </div>

                <h6>General notes</h6>

                <form action="{{ $project->path() }}" method="post">
                    @csrf
                    @method('patch')

                    <div class="input-group">
                        <textarea name="notes" rows="5" placeholder="Add some notes here..." class="form-control">{{ $project->notes }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </form>
            </div>

            <div class="col-4">
                <div class="card mt-4">
                    <div class="card-body">
                        <h5 class="card-title">{{ $project->title }}</h5>
                        <p class="card-text">{{ $project->description }}</p>
                        <p class="card-text">
                            <small class="text-muted">Last updated: {{ $project->updated_at->diffForHumans() }}</small>
                        </p>
                    </div>
                </div>

                <div class="card mt-4 mb-4">
                    <div class="card-body">
                        <h5 class="card-title">Invite a user</h5>
                        <form action="/projects/{{ $project->id }}/invitations" method="post">
                            @csrf

                            <div class="input-group mt-3">
                                <input type="email" class="form-control{{ $errors->invitations->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="E-mail">
                                <button type="submit" class="btn btn-primary rounded-end">Invite</button>

                                @error('email', 'invitations')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </form>
                    </div>
                </div>

                @include('projects.activity.card')
            </div>
        </div>

    </div>
@endsection
