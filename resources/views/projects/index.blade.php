@extends('layouts.app')

@section('content')
    <div class="container">

        @if ($projects->count() === 0)
            <p class="text-center mt-5 text-muted">There are no projects yet.</p>
        @else
            <div class="row" data-masonry='{ "percentPosition": true }'>
                @foreach ($projects as $project)
                    <div class="col-sm-6 col-lg-4 mb-4">
                        <a class="card project-card m-2" href="{{ $project->path() }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ Str::limit($project->description) }}</p>
                                <p class="card-text">
                                    <small class="text-muted">Last updated {{ $project->updated_at->diffForHumans() }}</small>
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif

    </div>
@endsection
