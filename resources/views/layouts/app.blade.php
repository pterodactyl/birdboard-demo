<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" >
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <svg width="32" height="22" viewBox="0 0 112 88" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M44.2941 64.0588C44.2941 61.0392 43.2156 58.6862 41.0588 57C38.9411 55.3137 35.196 53.5686 29.8235 51.7647C24.451 49.9607 20.0588 48.2156 16.647 46.5294C5.54901 41.0784 -2.49926e-06 33.5882 0 24.0588C1.24449e-06 19.3137 1.37255 15.1372 4.11765 11.5294C6.90196 7.88234 10.8235 5.05881 15.8823 3.05882C20.9412 1.0196 26.6274 -1.65589e-06 32.9412 0C39.098 1.61475e-06 44.6078 1.09804 49.4706 3.29412C51.8596 4.3644 53.9878 5.65822 55.8552 7.1756C57.1081 6.30207 58.4569 5.5181 59.9017 4.8237C64.9605 2.39233 70.9409 1.17665 77.8429 1.17665C88.2742 1.17665 96.4899 3.88253 102.49 9.2943C108.49 14.7061 111.549 22.1178 111.666 31.5296L111.666 88.0001L90.9016 88.0001L90.9017 31.1178C90.6664 21.7453 86.3134 17.059 77.8429 17.059C73.5683 17.059 70.3331 18.2355 68.137 20.5884C65.9409 22.9413 64.8429 26.7649 64.8429 32.059L64.8429 46.1973C61.4265 42.0781 56.7454 38.6323 51.0671 35.8424C49.0413 34.8414 46.7467 33.843 44.1958 32.8445V31.9413C44.1958 30.085 44.3113 28.3006 44.5424 26.5882H44.3529C44.3529 23.098 43.2745 20.3921 41.1176 18.4706C39 16.549 36.1176 15.5882 32.4706 15.5882C28.7843 15.5882 25.8627 16.4117 23.7059 18.0588C21.5882 19.6666 20.5294 21.7255 20.5294 24.2353C20.5294 26.4313 21.7059 28.4313 24.0588 30.2353C26.4117 32 30.549 33.8431 36.4706 35.7647C42.3921 37.647 47.2549 39.6862 51.0588 41.8823C60.3137 47.2157 64.9411 54.5686 64.9411 63.9411C64.9411 71.4313 62.1176 77.3137 56.4705 81.5882C50.8235 85.8627 43.0784 87.9999 33.2352 87.9999L33.2353 72.4705C36.647 72.4705 39.3333 71.745 41.2941 70.2941C43.2941 68.8039 44.2941 66.7254 44.2941 64.0588Z" fill="url(#paint0_linear)"/>
                        <defs>
                            <linearGradient id="paint0_linear" x1="0" y1="44.0004" x2="111.666" y2="44.0004" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#1076FD"/>
                                <stop offset="1" stop-color="#FF05DD"/>
                            </linearGradient>
                        </defs>
                    </svg>

                    {{ config('app.name', 'Birdboard') }}
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto align-items-center">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <a href="/create" class="btn btn-outline-primary btn-sm me-3">Add project</a>

                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="rounded" src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}?s=24" alt="{{ Auth::user()->name }}">

                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>
